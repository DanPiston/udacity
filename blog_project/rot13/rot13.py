import os

import jinja2
import webapp2
from codecs import encode

template_dir = os.path.join(os.path.dirname(__file__), 'templates')
jinja_env = jinja2.Environment(loader = jinja2.FileSystemLoader(template_dir), autoescape = True)

class Handler(webapp2.RequestHandler):
    def write(self, *a, **kw):
        self.response.out.write(*a, **kw)

    def render_str(self, template, **params):
        t = jinja_env.get_template(template)
        return t.render(params)

    def render(self, template, **kw):
        self.write(self.render_str(template, **kw))

class MainPage(Handler):
    def get(self):
        self.render("rotting.html")

    def post(self):
        text = self.request.get("text")
        rotted_text = encode(text, 'rot_13')
        self.render("rotting.html", text=rotted_text)

app = webapp2.WSGIApplication([('/rot13', MainPage),
                              ],
                              debug=True)
