import os
import re
import jinja2
import webapp2


# Jinja2 variables
template_dir = os.path.join(os.path.dirname(__file__), 'templates')
jinja_env = jinja2.Environment(loader = jinja2.FileSystemLoader(template_dir), autoescape = True)

# Regex used for checking submited data
user_re = re.compile(r"^[a-zA-Z0-9_-]{3,20}$")
pass_re = re.compile(r"^.{3,20}$")
email_re = re.compile(r"^[\S]+@[\S]+.[\S]+$")


class Handler(webapp2.RequestHandler):
    def write(self, *a, **kw):
        self.response.out.write(*a, **kw)

    def render_str(self, template, **params):
        t = jinja_env.get_template(template)
        return t.render(params)

    def render(self, template, **kw):
        self.write(self.render_str(template, **kw))

class MainPage(Handler):
    def get(self):
        self.render("signupform.html")

# Checks to see if the username matches
    def check_username(self, username):
        if not user_re.match(username):
            return True

    def check_pass(self, password):
        if not pass_re.match(password):
            return True

    def check_match_pass(self, password, verify):
        if password != verify:
            return True

    def check_email(self, email):
        if email and not email_re.match(email):
            return True

    def post(self):
        username = self.request.get("username")
        password = self.request.get("password")
        verify = self.request.get("verify")
        email = self.request.get("email")
        if self.check_username(username) != True and self.check_pass(password) != True and password == verify and self.check_email(email) != True:
            self.redirect("/welcome?username={}".format(username))
        else:
            self.render("signupform.html",
                        username=username,
                        email=email,
                        name_error=self.check_username(username),
                        pass_error=self.check_pass(password),
                        valid_pass_error=self.check_match_pass(password, verify),
                        email_error=self.check_email(email))

class Welcome(Handler):
    def get(self):
        username = self.request.get("username")
        self.render("welcome.html", username=username)

app = webapp2.WSGIApplication([('/signup', MainPage),
                               ('/welcome', Welcome),
                              ],
                              debug=True)
