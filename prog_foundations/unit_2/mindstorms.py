import turtle

def make_screen():
    window = turtle.Screen()
    window.bgcolor('red')
    draw_square()
    #draw_circle()
    #draw_triangle()
    window.exitonclick()

def draw_square():
    brad = turtle.Turtle()
    brad.shape("turtle")
    brad.color("orange")

    for squares in range(37):
        brad.right(10)
        for turns in range(4):
            brad.forward(100)
            brad.right(90)
            turns += 1
        squares += 1

def draw_circle():
    angie = turtle.Turtle()
    angie.shape('arrow')
    angie.color('blue')
    angie.circle(100)

def draw_triangle():
    ted = turtle.Turtle()
    ted.shape('turtle')
    ted.color('white')

    for turns in range(3):
        ted.right(120)
        ted.forward(75)
        turns += 1
make_screen()
