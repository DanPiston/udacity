import turtle

def make_drawing():
    window = turtle.Screen()
    window.bgcolor('white')
    draw_stem()
    draw_petals()
    draw_center()

    window.exitonclick()


def draw_stem():
    stem = turtle.Turtle()
    stem.color('green')
    stem.width(5)
    stem.right(90)
    stem.forward(175)

def draw_petals():
    petal = turtle.Turtle()
    petal.color('red')
    for petals in range(72):
        petal.right(5)
        for turns in range(2):
            petal.forward(60)
            petal.right(45)
            petal.forward(60)
            petal.right(135)
            turns += 1
        petals += 1
def draw_center():
    center = turtle.Turtle()
    center.color('black')
    for turns in range(60):
        center.right(6)
        center.circle(7)
        turns += 1


make_drawing()
