import urllib

def read_text():
    quotes = open('/home/dp/Code/udacity/prog_foundations/unit_2/movie_quotes.txt')
    contents_of_file = quotes.read()
    #print(contents_of_file)
    quotes.close()
    check_profanity(contents_of_file)

def check_profanity(text_to_check):
    connection = urllib.urlopen("http://www.wdylike.appspot.com/?q=" +text_to_check)
    output = connection.read()
    #print(output)
    connection.close()
    if 'true' in output:
        print('Profane word detected!')
    elif 'false' in output:
        print('All Clear!')
    else:
        print('Could not read document properly')

read_text()
