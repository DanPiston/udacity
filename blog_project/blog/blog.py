import os
import webapp2
import jinja2

from google.appengine.ext import db

template_dir = os.path.join(os.path.dirname(__file__), 'templates')
jinja_env = jinja2.Environment(loader = jinja2.FileSystemLoader(template_dir),
                               autoescape = True)

class Handler(webapp2.RequestHandler):
    def write(self, *a, **kw):
        self.response.out.write(*a, **kw)

    def render_str(self, template, **params):
        t = jinja_env.get_template(template)
        return t.render(params)
    def render(self, template, **kw):
        self.write(self.render_str(template, **kw))

class Blog(db.Model):
    title = db.StringProperty(required = True)
    blog_body = db.TextProperty(required = True)
    created = db.DateTimeProperty(auto_now_add = True)

class MainPage(Handler):
    def render_blog(self, title="", blog_body="", error=""):
        posts = db.GqlQuery("SELECT * FROM Blog "
                           "ORDER BY created DESC ")

        self.render("blog.html", title=title, blog_body=blog_body, error=error, posts=posts)

    def get(self):
        self.render_blog()
    
class NewPost(Handler):
    def get(self):
        self.render("newpost.html")

    def post(self):
        title = self.request.get("blog_title")
        blog_body = self.request.get("blog_body")

        if title and blog_body:
            b = Blog(title=title, blog_body=blog_body)
            b.put()

            self.redirect("/blog")
        else:
            error = "we need both title and a post!"
            self.render_blog(error=error, title=title, blog_body=blog_body)

app = webapp2.WSGIApplication([
    ('/blog', MainPage),
    ('/blog/newpost', NewPost)
    ], debug = True)
