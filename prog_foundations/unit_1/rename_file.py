import os

def rename_files():
    file_list = os.listdir(r'/home/dp/Code/udacity/prog_foundations/unit_1/prank')
    saved_path = os.getcwd()
    print(f'Current working dir is {saved_path}')
    os.chdir(r'/home/dp/Code/udacity/prog_foundations/unit_1/prank/')
    for file_name in file_list:
        os.rename(file_name, file_name.strip('1234567890'))
    os.chdir(saved_path)

rename_files()
