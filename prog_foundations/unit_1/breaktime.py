import time
import webbrowser

total_breaks = 3
break_count = 0
print("Started at: " + time.ctime())
while break_count < total_breaks:
    time.sleep(1)
    webbrowser.open("http://www.google.com")
    break_count += 1

print("Stopped at: " + time.ctime())
